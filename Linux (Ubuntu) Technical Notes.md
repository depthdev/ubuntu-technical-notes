Linux (Ubuntu) Technical Notes
===

NOTE: Some examples use Kubuntu, but apply to other Ubuntu or Debian-based distros as well.


## General
* Some notable differences from Windows:
    * Forward slashes are used for ALL paths (instead of back slashes for system navigation)
    * LF (LineFeed)s are used instead of CRLF (Carriage-Return LineFeed) for new lines
    * File names support more types of characters
    * There isn't the 256 bit character file name / file directory path name limit
    * The terminal uses Bash instead of the Command Line
    * You only need Bash (Terminal) commands instead of Command Line and PowerShell's often more complicated syntaxes
    * Bash leverages a lot of Regex or Regex-like syntaxes
    * For the most part, Antivirus isn't needed [(https://help.ubuntu.com/community/Antivirus)](https://help.ubuntu.com/community/Antivirus)
    * `/` is the same as `C:\` in Windows
    * `~` or (`/home/adam`) is the same as `C:\Users\adamc` in Windows
* Whenever you make changes to a system file, ALWAYS create a backup copy!


## Drive Setup
1. Get a Linux distro (distribution) ISO like [Ubuntu](https://www.ubuntu.com/download/desktop), [Kubuntu](https://kubuntu.org/getkubuntu/), or one of the [Ubuntu flavours](https://www.ubuntu.com/download/flavours)
    * Verify the Checksums match. See "Checksums" section of [https://kubuntu.org/alternative-downloads](https://kubuntu.org/alternative-downloads).
        * `d22271e7702d87eb856a83964c91be7a6f73af19b9f0b76ad35ab9f831c72c73` (kubuntu-18.04.1-desktop-amd64.iso SHA256)
        * `sha256sum kubuntu-18.04.1-desktop-amd64.iso` (Gets sha256 hash of ISO)
        * OR `md5sum kubuntu-18.04.1-desktop-amd64.iso` (Gets md5 hash of ISO)
        * `[ "iso-hash-string-here" == "hash-string-from-website-here" ] && echo "true" || echo "false"` (Compare hashes)
        * `sudo dd if=/home/adam/Desktop/kubuntu-18.04.1-desktop-amd64.iso of=/dev/sdc bs=4096 status=progress && sync` (Makes a bootable flash drive on drive 3/sd{c})
1. If dual booting, install Windows first, even if on a separate drive
    * Install using entire drive with or without LVM, and with or without encryption
        * Or, for a manual install or a shared drive, create 3 partitions on the drive of choice:
            1. Remember that `sda` is the first drive, `sdb` is the second, `sda3` is the third partition on the first drive
            1. Do a "manual" setup if using a dual boot especially.
            1. `/` Root, make sure it has at least 20GB and formatted as `ext4`.
            1. `/home` Give it the rest of the room (minus the swap) and formatted as `ext4`.
            1. `swap` (Virtual memory) Give it at least half of your ram's size and formatted as `swap`.
    * Don't try to share files between OS--they can get corrupted. Copy files or directories of needed.


## BIOS Setup
1. If the boot menu doesn't show (where you get to chose which OS you want to boot) and Windows always immediately starts loading
	1. Go into your Bios
	1. Set "Boot mode select" to "UEFI with CSM", "UEFI", or "Legacy". Should be set "UEFI with CSM"
	1. Set "Fast Boot" to "Disabled" (may happen automatically with previous change)
	1. Make the primary (sda) hard drive be first in the boot order
	1. If you have a "EUFI Hard Disk Drive BBS Priorities" section, select it and set Ubuntu first in that boot order instead of "Windows Boot Manager"
	1. F10 to save and quit!


## Boot/Grub Config
1. Set Windows as default boot loader
	1. `sudo apt install vim`
    1. `sudo vim /etc/default/grub` (Change `GRUB_DEFAULT=0` to `GRUB_DEFAULT=2` for a zero-based index in the order it appears on the boot menu)
        1. Alternatively, you could do `sudo vim /etc/default/grub` and change `GRUB_DEFAULT=0` to `GRUB_DEFAULT="Windows Boot Manager (on /dev/sdb2)"` or whatever you find doing `grep menuentry /boot/grub/grub.cfg` (Find menuentry starting with 'Windows...', such as `Windows Boot Manager (on /dev/sdb2)`, and copy it without the quotes)
    1. `Esc` then `:wq` or `:x` (Save & quit)
    1. `sudo update-grub`
1. `timedatectl set-local-rtc 1` (Set times as local instead of UTC)
    * This resolves an issue with Windows time being off because of default UTC in Linux.
    * NOTE: This only applies to dual boot setups.


## Install Packages
* `apt list --installed` (List all installed packages)
* `sudo apt-get update` (Update package manager & resolve 404 package errors during install)
* `sudo apt-get packagename` (Uninstall a package)
* `sudo apt-get --purge autoremove package-name` or `sudo apt remove package-name && sudo apt purge package-name && sudo apt autoremove` (Deep uninstall a package)
* `sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade` (Update the OS and all packages)
* `sudo apt install git`
* `sudo apt install nodejs` (v8 only; see below for v10)
* `sudo apt install npm` (v8 only; see below for v10)
* `sudo apt install vim`
* `sudo apt install curl`
* `sudo apt install kvpm` (Logical volume manager. Only for Kubuntu with LVM disk setup!)
* Node.js v10 & NPM
	0. Remove default v8 nodejs installation: `sudo apt-get purge --auto-remove nodejs`
	0. `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -` ([Official download site](https://github.com/nodesource/distributions/blob/master/README.md#debinstall))
	0. `sudo apt-get install -y nodejs`
	0. Verify installations: `node -v` and `npm -v`
* [Visual Studio Code](https://code.visualstudio.com/) (Download .deb versionand double-click the .dep file to install)
* Google Chrome for download on their website


## KDE Plasma (Desktop Environment) Config
* Change to using nVidia
    1. Check for 3rd party driver updates (should be prompted after initial install or first reboot)
    1. System Settings > Hardware > Driver Manager
    1. Select "Using NVIDIA driver metapackage from nvidia-driver-390 (Recommended Driver)"
    1. NOTE: This takes a couple minutes even on my fast box so be patient
        * You may need to make the window bigger to see the progress bar
* Prevent UI zoom:
    1. System Settings > Appearance > Fonts
    1. Check "Force fonts DPI: 96"
    1. Click "Apply" on the bottom right
    1. Reboot
* Enable NumLock by default
    1. System Settings > Hardware > Input Devices > Hardware (tab).
    1. Select "Turn on" under the "NumLock on Plasma Startup" section.
    1. Click "Apply".
* Keyboard shortcuts:
    1. System Settings > Workspace > Shortcuts > Global Shortcuts
    1. Click the "+" symbol and find the application you want to bind to and click "OK"
    1. Click on the "Launch {the app name}" line in the cell under the "Global" column
    1. Click on Custom > None which will change it to "Input ..."
    1. Type the keybinding such as:
        * `Ctrl + Alt + t` (Terminal/Konsole)
        * `Ctrl + Alt + f` (Firefox)
    1. Click "Apply"
    1. For the Dolphin File Manager, Sublime Text 3, and Vim:
        1. Select on Custom Shortcuts
        1. Select "Edit" at the bottom
        1. Select New > Global Shortcut > Command/URL
        1. Write the name of the shortcut "Dolphin File Manager"
        1. Click on the new shortcut
        1. Select the "Action" tab
        1. Type the Command/URL as `dolphin`, or `subl`
        1. Select the "Trigger" tab
        1. Click on Shortcut: None which will change it to "Input ..."
        1. As an example, type `Ctrl + Alt + d` (Directory/Dolphin File Manager or `Meta(Windows Key) + e` to mimic Windows Explorer shortcut)
        1. Click "Apply" on the bottom right
* Prevent preserving previous session (opened apps on the desktop):
    1. System Settings > Workspace > Startup and Shutdown > Desktop Session
    1. Change "On Login" radio button to "Start with an empty session"
    1. Click "Apply"
* Remove top-right desktop toolbox:
    1. Right-click desktop and select "Configure Desktop".
    1. Select "Tweaks" in the lefthand menu.
    1. Un-check "Show the desktop toolbox" and select "OK".
    1. NOTE: This will have to be done for each monitor.
* Change desktop background
	1. Right-click each monitor's desktop background
	1. Select "Configure Desktop"
	1. Select "Wallpaper" on the left navigation
	1. Select "+ Add Image..." button to choose a new image
	1. Set "Positioning:" to "Scaled and Cropped" to prevent image distortion
	1. Click "Apply"
* Change lock screen background
	1. Settings > Workspace > Desktop Behavior > Screen Locking
	1. Select "Appearance" tab
	1. Select "+ Add Image..." button to choose a new image
	1. Set "Positioning:" to "Scaled and Cropped" to prevent image distortion
	1. Click "Apply"
* Change user image
	1. Click on the KDE "Start" button
	1. Click on your user image
	1. Click on your user image again in the "User Manager -- System Settings Module" to change the image.
	1. Click "Apply"
* Window moving keyboard shortcuts
    * `Window + PgUp` (Maximize the window)
    * `Window + PgDn` (Minimize the window)
    * `Window + Arrow Up/Down/Left/Right` (Move current window to quadrant of any monitor)
* Dolphin File Manager
    * `ctrl+h` (Toggle showing hidden dot files in the Dolphin file manager)
* Make KDE Plasma desktop environment work if it's not fully loading:
    * `Alt + F2` > `plasmashell`


## Utilities
* `timedatectl set-local-rtc 1` (Set times as local instead of UTC)
    * This resolves an issue with Windows time being off because of default UTC in Linux.
    * NOTE: This only applies to dual boot setups.
* `Alt + F2` (Quick search to open or run most any application)
* `ctrl + alt + t` (Open a new Terminal, may need to be setup in Kubuntu first)
* `sudo ` (Putting "Super User Do _this_" in front of any command gives "Administrative" priviledge to run a command, similar to "Run as Administrator" on Windows)
* `Ctrl + Shift + v` (Paste)
* `Ctrl + Shift + f` (Find)
* `Ctrl + Alt + Escape` (Turns the cursor into a red skull, which can then be used to click on an app and force quit it)
* `poweroff` (Shutdown)
* `reboot` (Restart)
* `systemctl suspend` (Sleep/Standby)
* `systemctl hibernate` (Hibernate)
* `lsb_release -a` (OS version)
* `lscpu` (View CPU specs)
* `lsblk` or `lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL` (View drives and partitions)
* `watch -n 5 free -m` (View memory usage)
* `sudo dd if=/dev/zero of=/dev/sda bs=4096 status=progress` (Zero fill first hard drive {sd[a]}. if=Input File; of=Output File; bs=allocation unit size)
* `sudo dd if=/dev/urandom of=/dev/sda bs=4096 status=progress` (Random data fill first hard drive {sd[a]}. if=Input File; of=Output File; bs=allocation unit size)
* `sudo dd if=/home/adam/Desktop/kubuntu-18.04.1-desktop-amd64.iso of=/dev/sdc bs=4096 status=progress && sync` (Makes a bootable flash drive on drive 3 {c})
	* Alternative (such as for Raspbian): `sudo dd if=/home/adam/Desktop/kubuntu-18.04.1-desktop-amd64.iso of=/dev/sdc bs=4M conv=fsync status=progress && sync`
* `sudo dd if=/dev/sda of=/dev/sdb bs=64K conv=noerror,sync status=progress` (Clones a hard drive or partition)
* `sudo dd if=/dev/sda of=/path/to/backup.img bs=4M conv=sync status=progress` (Creates a backup image of a hard drive or partition)
	* `sudo dd if=/path/to/backup.img of=/dev/sda bs=4M status=progress` (Restores a backup image)
* `sudo dd if=/dev/sda conv=sync,noerror bs=64K | gzip -c  > /path/to/backup.img.gz` (Creates a gzipped backup image of a hard drive or partition)
	* `gunzip -c /path/to/backup.img.gz | dd of=/dev/sda` (Restores a gzipped backup image)
* `genisoimage -o ~/Desktop/some-image.iso ~/Desktop/some-directory` (Creates an ISO out of a directory)
* `sha256sum kubuntu-18.04.1-desktop-amd64.iso` (Gets sha256 hash of ISO)
* `md5sum kubuntu-18.04.1-desktop-amd64.iso` (Gets md5 hash of ISO)
* `ssh-keygen -t rsa -C "my.email@example.com"` (Create an SSH key at the default location with the option to change the location)


## Directory Navigation
* `/` (The "root" directory; like the C:\ drive in Windows)
* `~` (Shorthand for the `/<username>/home` directory, aka `root > your-username > home`; like the Windows equivalent of `C:\Users\<username>`)
* `cd ~/Desktop` (Short for changing to root (/), home (home), user directory (adam), desktop (Desktop))
* `cd /some/path` (Switch to another directory)
* `ls` (List files and directories in directory)
* `ls -a` (List files and directories in directory AND show hidden dot files)
* `Tab` (To autocomplete the name of the file you're typing)
* `Ctrl + c` (Get out of a shell process like npm scripts, or password prompt)
* `Ctrl + Arrow Left/Right` (Place the cursor at the beginning/end of the next word)
* `Ctrl + Arrow Left/Right + Shift` (Selects words at a time)
* `Shift + Arrow Left/Right` (Selects a character)
* `Arrow Up/Down` (Displays previous commands in historical order)
* `Ctrl + Shift + f` (Find string in terminal output)


## Directory and File Manipulation
* `ln -s /link/to/this/directory <optionally/link/that/to/an/already/created/directory>` (Creates a symbolic link a.k.a. "symlink" to a directory with the same name. NOTE: This seemed to corrupt files on the Windows side.)
* `mkdir ~/Desktop/new-folder` (Make a new folder on the Desktop)
* `mv /some/file.txt /somewhere/else/file.txt` (Move a file/directory to another directory)
* `mv /some/thing /some/thingy` (Rename a file/directory and optionally cause it to move if the name is changed)
* `rm /some/file.txt` (Remove a file)
* `rm -r` (Remove a directory and all files recursively) or `rm -rf` (to force it) or `sudo rm -rf` (to really force it)
* `cp /some/file.txt /some/where/else/file.txt` (Copy a file into the new location, and optionally rename it)
* `cp -a /some/directory /some/where/else/directory` (Copy a directory recursively into a new location, and optionally rename it)
* `rename 's/^images\/(.+)/sub\/$1.jpg/s' images/*.jpg` (Rename files in a given directory and move them if the path is changed. In this case, get all PNGs from the "images" directory, and move them to a sub directory called "sub".)
    * `for x in *.jpg;do mv $x sub/${x%.jpg}sub.jpg;done` (As an alternative)
* `egrep -ir --include=*.{html,css,js} "(document.querySelectorAll|window.setTimeout)" .` (Find files with certain strings in them. In this case, looking for "document.querySelectorAll" and "window.setTimeout" case insensitively ("-i") in the current directory (".") and recursively ("-r") of HTML, CSS, and JS files.)
* `chmod +x ./some-app.run` > `./some-app.run` (Make a ".run" file executable, then run it. Make sure a type of path is included. May need to be run with `sudo` before it.)


## Shebang/sha-bang/hashbang for Bash
* `#!/usr/bin/env node --experimental-modules --no-warnings` (Executing Node in this example for a Node CLI)
    * OR, the following two lines as two seperate lines at the top of a file to support Linux (including Windows and Mac OS X)
    * `#!/bin/sh`
    * `":" //# Linux workaround; exec /usr/bin/env node --experimental-modules --no-warnings "$0" "$@"`
        * This child process is a workaround for Linux distros until the latest GNU Coreutils is supported to allow multiple arguments in a shebang via `#!/usr/bin/env node --experimental-modules --no-warnings`.
        * See https://www.gnu.org/software/coreutils/manual/html_node/env-invocation.html#g_t_002dS_002f_002d_002dsplit_002dstring-usage-in-scripts
        * Windows and Mac OS X support multiple arguments in a shebang, but also function with this workaround.
        * Credit and explanation for the workaround: http://sambal.org/2014/02/passing-options-node-shebang-line/


## VIM
* `sudo apt install vim` (Install Vim)
* `vim` (Launch Vim)
* `vim /path/to/file.txt` (Open file in Vim, or creates a new file and opens it in Vim)
* `:w !sudo tee % > /dev/null` (Force give yourself permission to save a file in a protected directory)
    * `sudo vim /some/new/file.txt` (Or start the file this way)


## Sublime Text
* Install:
    1. Extract Sublime Text 3 directories (Sublime Text Build 3176 x64 (Linux - Licensed & Loaded).tar.gz)
    1. Place "sublime-text-3" in the hidden directory of `~/.config`
    1. Place "sublime\_text\_3" in the root directory of `/opt`
    1. `sudo ln -s /opt/sublime_text_3/sublime_text /usr/bin/subl` (Creates terminal shortcut of `subl`)
* `subl` (Launch Sublime Text 3)
* `subl /path/to/file` (Open file in Sublime Text 3)


## Re-install LibreOffice (Only if needed!)
1. `sudo apt-get remove --purge libreoffice*`
1. `sudo apt-get clean`
1. `sudo apt-get autoremove`
1. `sudo apt-get install libreoffice` (Install LibreOffice)
1. `sudo apt-get install libreoffice-style-breeze` (Install KDE default Breeze style)


## NVIDIA (Shouldn't need to mess with this!)
* Use the nVidia app or:
    * `sudo nvidia-settings` (Open nVidia settings)
        1. WARNING: This may royally mess things up! Only do it if you need to! Uninstall nVidia (WARNING! Nouveau display driver can prevent a successful re-installation of nVidia):
        1. `sudo apt-get remove --purge nvidia-*`
        1. `sudo rm /etc/X11/xorg.conf`
